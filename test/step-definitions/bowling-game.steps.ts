import { before, binding, given, then, when } from 'cucumber-tsflow';
import { assert } from 'chai';
import { BowlingGame } from '../../src/game/BowlingGame';
import { AmericanBowlingGameParser } from '../../src/parser/AmericanBowlingGameParser';
import { AmericanBowlingRules } from '../../src/rules/AmericanBowlingRules';

@binding()
export class BowlingGameWithoutBonusSteps {
    private score: number = 0;
    private game!: BowlingGame;
    private bowlingGameParser!: AmericanBowlingGameParser;

    @before()
    public beforeAll() {
        const rules = new AmericanBowlingRules();
        this.game = new BowlingGame(rules);
        this.bowlingGameParser = new AmericanBowlingGameParser(rules);
    }

    @given(/(\d*) bowling frames, (\d*) rolls with sequence : \((.*)\)/)
    public givenASimpleGameWithKnockedDownPins(frames: number, rolls: number, sequence: string) {
        this.game = this.bowlingGameParser.parse(sequence);
    }

    @when('The game is over and the score is computed')
    public computedScore() {
        this.score = this.game.score();
    }

    @then(/The score should equal to (\d*)/)
    public scoreShouldEqualTo(expectedScore: number) {
        assert.equal(this.score, expectedScore);
    }
}
