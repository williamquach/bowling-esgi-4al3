Feature: Bowling Game with end frame bonus

    @PerfectGame
    Scenario: Player do the perfect game
        Given 10 bowling frames, 12 rolls with sequence : (X X X X X X X X X X X X)
        When The game is over and the score is computed
        Then The score should equal to 300

    @AllStrikesWithStrikeAndSpareDuringBonus
    Scenario: Player do only strikes, with a strike and spare during bonus
        Given 10 bowling frames, 12 rolls with sequence : (X X X X X X X X X X 7/)
        When The game is over and the score is computed
        Then The score should equal to 287

    @AllStrikesWithOneStrikeDuringBonus
    Scenario: Player do only strikes, with a strike and 7 knocked down pins during bonus
        Given 10 bowling frames, 12 rolls with sequence : (X X X X X X X X X X 7-)
        When The game is over and the score is computed
        Then The score should equal to 284

    @AllStrikesWithOneStrikeAndTwoFailsDuringBonus
    Scenario: Player do only strikes, with one strike then 0 knocked down pins during bonus
        Given 10 bowling frames, 12 rolls with sequence : (X X X X X X X X X X --)
        When The game is over and the score is computed
        Then The score should equal to 270

    @AllStrikesWithTwoStrikesAAndFailDuringBonus
    Scenario: Player do only strikes, with 2 strikes then 0 knocked down pins during bonus
        Given 10 bowling frames, 12 rolls with sequence : (X X X X X X X X X X X -)
        When The game is over and the score is computed
        Then The score should equal to 290

    @AllSparesWithSpareDuringBonus
    Scenario: Player do only spares, with a spare and 5 knocked down pins during bonus
        Given 10 bowling frames, 21 rolls with sequence : (5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5)
        When The game is over and the score is computed
        Then The score should equal to 150

    @AllSparesWithSpareAndFailDuringBonus
    Scenario: Player do only spares with a spare and 0 knocked down pins during bonus
        Given 10 bowling frames, 21 rolls with sequence : (5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ -)
        When The game is over and the score is computed
        Then The score should equal to 145

    @AllSparesWithSpareAndStrikeDuringBonus
    Scenario: Player do only spares with a spare and 0 knocked down pins during bonus
        Given 10 bowling frames, 21 rolls with sequence : (5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ X)
        When The game is over and the score is computed
        Then The score should equal to 155