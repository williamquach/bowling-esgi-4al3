Feature: Bowling Game without end frame bonus

    @RegularPlayer
    Scenario: Some of the pins are knocked down on each frame
        Given 10 bowling frames, 20 rolls with sequence : (9- 9- 9- 9- 9- 9- 9- 9- 9- 9-)
        When The game is over and the score is computed
        Then The score should equal to 90

    @IrregularPlayer
    Scenario: Player knocked down pins on half of frames
        Given 10 bowling frames, 20 rolls with sequence : (5- 5- 5- 5- 5- -- -- -- -- --)
        When The game is over and the score is computed
        Then The score should equal to 25

    @BadPlayer
    Scenario: Player knocked down none pins
        Given 10 bowling frames, 20 rolls with sequence : (-- -- -- -- -- -- -- -- -- --)
        When The game is over and the score is computed
        Then The score should equal to 0

    @ConstantBadPlayer
    Scenario: Player knocked down one pin per roll
        Given 10 bowling frames, 20 rolls with sequence : (11 11 11 11 11 11 11 11 11 11)
        When The game is over and the score is computed
        Then The score should equal to 20

    @WithSpares
    Scenario: Player always do spares except for last frame
        Given 10 bowling frames, 20 rolls with sequence : (5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ --)
        When The game is over and the score is computed
        Then The score should equal to 130

    @WithStrikes
    Scenario: Player always do strikes except for last frame
        Given 10 bowling frames, 11 rolls with sequence : (X X X X X X X X X --)
        When The game is over and the score is computed
        Then The score should equal to 240
