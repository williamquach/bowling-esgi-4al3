import { AmericanBowlingGameParser } from './src/parser/AmericanBowlingGameParser';
import { AmericanBowlingRules } from './src/rules/AmericanBowlingRules';

console.log('=============================');
console.log('Welcome to the Bowling Game !\n');
console.log('=============================');

const strikes = 'X X X X X X X X X X X X'; // (12 rolls: 12 strikes) = 10 frames * 30 points = 300
const normal = '9- 9- 9- 9- 9- 9- 9- 9- 9- 9-'; // (20 rolls: 10 pairs of 9 and miss) = 10 frames * 9 points = 90
const spares = '5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5'; // (21 rolls: 10 pairs of 5 and spare, with a final 5) = 10 frames * 15 points = 150

const bowlingGameParser = new AmericanBowlingGameParser(new AmericanBowlingRules());
console.log(`With '${strikes}' - score = ${bowlingGameParser.parse(strikes).score()}`);
console.log(`With '${normal}' - score = ${bowlingGameParser.parse(normal).score()}`);
console.log(`With '${spares}' - score = ${bowlingGameParser.parse(spares).score()}`);
console.log('=============================');
