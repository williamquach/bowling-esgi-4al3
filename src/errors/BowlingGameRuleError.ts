export class BowlingGameRuleError extends Error {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, BowlingGameRuleError.prototype);
    }
}
