export class Roll {
    constructor(public representation: string, public points: number) {}

    toString(): string {
        return `Representation : ${this.representation} | points : ${this.points}`;
    }
}
