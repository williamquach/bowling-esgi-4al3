import { BowlingGame } from '../game/BowlingGame';
import { AmericanBowlingRules } from '../rules/AmericanBowlingRules';
import { BowlingRules } from '../rules/BowlingRules';
import { BowlingGameParser } from './BowlingGameParser';

export class AmericanBowlingGameParser implements BowlingGameParser {
    constructor(private rules: BowlingRules) {}

    public parse(gameToParse: string): BowlingGame {
        const gameFrames: string[] = gameToParse.split(' ');
        return this.computeGameRolls(gameFrames);
    }

    private computeGameRolls(splitGame: string[]): BowlingGame {
        const bowlingGame = new BowlingGame(new AmericanBowlingRules());
        splitGame.forEach(roll => {
            if (this.isBonusFrameSpare(roll)) {
                this.rollABonusFrameSpare(bowlingGame, roll);
            } else if (this.isStrike(roll)) {
                this.rollAStrike(bowlingGame, roll);
            } else if (this.isSpare(roll)) {
                this.rollASpare(bowlingGame, roll);
            } else {
                this.rollACommon(bowlingGame, roll);
            }
        });
        return bowlingGame;
    }

    private isStrike(roll: string) {
        return roll === this.rules.STRIKE_SYMBOL;
    }

    private isSpare(roll: string) {
        return roll.includes(this.rules.SPARE_SYMBOL);
    }

    private isBonusFrameSpare(roll: string): boolean {
        return roll.length === 3 && this.isSpare(roll);
    }

    private static roll(bowlingGame: BowlingGame, representation: string, points: number): void {
        bowlingGame.roll(representation, points);
    }

    private rollAStrike(bowlingGame: BowlingGame, representation: string): void {
        AmericanBowlingGameParser.roll(bowlingGame, representation, this.rules.MAX_POINTS_BY_FRAME);
    }

    private rollASpare(bowlingGame: BowlingGame, representation: string): void {
        const framePoint = parseInt(representation[0]);
        AmericanBowlingGameParser.roll(
            bowlingGame,
            representation[0],
            !isNaN(framePoint) ? framePoint : this.rules.MIN_POINTS_BY_FRAME
        );
        AmericanBowlingGameParser.roll(bowlingGame, '/', this.rules.MAX_POINTS_BY_FRAME - framePoint);
    }

    private rollACommon(bowlingGame: BowlingGame, representation: string): void {
        const firstRollPoint = parseInt(representation[0]);
        const secondRollPoint = parseInt(representation[1]);
        AmericanBowlingGameParser.roll(
            bowlingGame,
            representation[0],
            isNaN(firstRollPoint) ? this.rules.MIN_POINTS_BY_FRAME : firstRollPoint
        );
        AmericanBowlingGameParser.roll(
            bowlingGame,
            representation[1],
            isNaN(secondRollPoint) ? this.rules.MIN_POINTS_BY_FRAME : secondRollPoint
        );
    }

    private rollABonusFrameSpare(bowlingGame: BowlingGame, representation: string): void {
        const lastRollPoints = parseInt(representation[2]);
        this.rollASpare(bowlingGame, representation);
        AmericanBowlingGameParser.roll(bowlingGame, representation[2], lastRollPoints);
    }
}
