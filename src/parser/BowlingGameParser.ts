import { BowlingGame } from '../game/BowlingGame';

export interface BowlingGameParser {
    parse(gameToParse: string): BowlingGame;
}
