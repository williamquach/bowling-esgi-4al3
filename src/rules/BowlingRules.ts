export interface BowlingRules {
    STRIKE_SYMBOL: string;
    SPARE_SYMBOL: string;
    NO_PIN_SYMBOL: string;

    MAX_POINTS_BY_FRAME: number;
    MIN_POINTS_BY_FRAME: number;

    NUMBER_OF_FRAMES: number;
    MAXIMUM_NUMBER_OF_ROLLS: number;
}
