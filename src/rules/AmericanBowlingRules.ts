import { BowlingRules } from './BowlingRules';

export class AmericanBowlingRules implements BowlingRules {
    public readonly STRIKE_SYMBOL = 'X';
    public readonly SPARE_SYMBOL = '/';
    public readonly NO_PIN_SYMBOL = '-';

    public readonly MAX_POINTS_BY_FRAME = 10;
    public readonly MIN_POINTS_BY_FRAME = 0;

    public readonly NUMBER_OF_FRAMES = 10;
    public readonly MAXIMUM_NUMBER_OF_ROLLS = 21;
}
