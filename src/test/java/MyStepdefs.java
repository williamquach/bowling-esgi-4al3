import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MyStepdefs {
    @Given("I have a configured Cucumber-JVM project")
    public void iHaveAConfiguredCucumberJVMProject() {
    }

    @When("I run it within my IDE")
    public void iRunItWithinMyIDE() {
    }

    @Then("I will be able to run connected step definitions")
    public void iWillBeAbleToRunConnectedStepDefinitions() {
    }
}
