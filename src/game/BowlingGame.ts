import { Roll } from '../Roll';
import { BowlingRules } from '../rules/BowlingRules';
import { BowlingGameRuleError } from '../errors/BowlingGameRuleError';

export class BowlingGame {
    private rolls: Roll[] = [];

    constructor(private rules: BowlingRules) {}

    roll(representation: string, framePoint: number): void {
        if (this.rolls.length > this.rules.MAXIMUM_NUMBER_OF_ROLLS) {
            throw new BowlingGameRuleError(`Number of roll cannot exceed ${this.rules.MAXIMUM_NUMBER_OF_ROLLS}`);
        }
        this.rolls.push(new Roll(representation, framePoint));
    }

    score(): number {
        let score: number = 0;
        let rollIndex = 0;
        for (let frameIndex = 0; frameIndex < this.rules.NUMBER_OF_FRAMES; frameIndex += 1) {
            let currentRoll = this.rolls[rollIndex];
            if (this.isLastFrame(frameIndex)) {
                return this.getLastFramePoints(rollIndex, score);
            }
            if (this.isStrike(currentRoll)) {
                score += this.getStrikePoints(currentRoll, rollIndex);
                rollIndex = BowlingGame.getIndexToNextFrame(rollIndex, 1);
            } else {
                score += this.getDoubleRollFramePoints(currentRoll, rollIndex);
                rollIndex = BowlingGame.getIndexToNextFrame(rollIndex, 2);
            }
        }
        return score;
    }

    private isLastFrame(frameIndex: number): boolean {
        return frameIndex === this.rules.NUMBER_OF_FRAMES - 1;
    }

    private isStrike(roll: Roll): boolean {
        return roll && roll.representation === this.rules.STRIKE_SYMBOL;
    }

    private isSpare(roll: Roll): boolean {
        return roll && roll.representation === this.rules.SPARE_SYMBOL;
    }

    private static getIndexToNextFrame(index: number, add: number) {
        return index + add;
    }

    private getStrikePoints(currentRoll: Roll, rollIndex: number): number {
        const nextRoll = this.rolls[rollIndex + 1];
        const secondRollAfterCurrent = this.rolls[rollIndex + 2];
        return currentRoll.points + nextRoll.points + secondRollAfterCurrent.points;
    }

    private getDoubleRollFramePoints(currentRoll: Roll, rollIndex: number): number {
        const secondRoll = this.rolls[rollIndex + 1];
        const firstRollOfNextFrame = this.rolls[rollIndex + 2];
        if (this.isSpare(secondRoll)) {
            return BowlingGame.getSparePoints(currentRoll, secondRoll, firstRollOfNextFrame);
        } else {
            return BowlingGame.getCommonFramePoints(currentRoll, secondRoll);
        }
    }

    private getLastFramePoints(rollIndex: number, score: number): number {
        for (let rollFromLastFrame = rollIndex; rollFromLastFrame < this.rolls.length; rollFromLastFrame += 1) {
            let currentRoll = this.rolls[rollFromLastFrame];
            score += currentRoll.points;
        }
        return score;
    }

    private static getSparePoints(currentRoll: Roll, secondRoll: Roll, firstRollOfNextFrame: Roll): number {
        return currentRoll.points + secondRoll.points + firstRollOfNextFrame.points;
    }

    private static getCommonFramePoints(currentRoll: Roll, secondRoll: Roll): number {
        return currentRoll.points + secondRoll.points;
    }

    toString(): string {
        let res = '';
        this.rolls.forEach((roll, index) => {
            res += `\nRoll n°${index + 1} :\t${roll.toString()}\n`;
        });
        return res;
    }
}
